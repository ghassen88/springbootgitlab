FROM openjdk:13-alpine
VOLUME /tmp
ADD /target/springbootgitlab-0.0.1-SNAPSHOT.jar springbootgitlab-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/springbootgitlab-0.0.1-SNAPSHOT.jar"]


